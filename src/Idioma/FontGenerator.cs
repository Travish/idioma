﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Media;
using Color = System.Drawing.Color;
using FontFamily = System.Drawing.FontFamily;

namespace Idioma
{
    internal class FontGenerator
    {
        private const string GenericChar = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|} ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ";
        private const string CyrillicChar = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ЂЃ‚ѓ„…†‡€‰Љ‹ЊЌЋЏђ‘’“”•–—™љ›њќћџ ЎўЈ¤Ґ¦§Ё©Є«¬­®Ї°±Ііґµ¶·ё№є»јЅѕїАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюя";

        public enum Charset { DefaultCharset, CyrillicCharset };

        private const string fontStringPrefix = "fonts/";

        private static byte[] StringToBytes(string str)
        {
            List<byte> bytes = new List<byte>(Encoding.ASCII.GetBytes(str));
            bytes.Add(0);
            return bytes.ToArray();
        }

        private static void WriteToFile(string DirectoryToDump, string FileName, Dictionary<short, RectangleF> CharsAndPositions,
            int TopOffset, int RightOffset, int BitmapWidth, int BitmapHeight, string FontName, string FontMaterial)
        {
            using (BinaryWriter writeFile = new BinaryWriter(File.Create(Path.Combine(DirectoryToDump, FileName))))
            {
                const int headerSize = 16;
                const int recordSize = 24;
                int fontNameOffset = (recordSize * CharsAndPositions.Count) + headerSize;
                int materialNameOffset = fontNameOffset + FontName.Length + 1;

                //Write the offset
                writeFile.Write(fontNameOffset);
                //Size in pixels
                writeFile.Write((int)20);
                //Number of entries
                writeFile.Write((int)CharsAndPositions.Count);
                //Other offset
                writeFile.Write(materialNameOffset);
                //Loop through and output entries
                foreach (short required in CharsAndPositions.Keys)
                {
                    //Write charactor code l,t,r,b
                    writeFile.Write((short)required);
                    //Check
                    if (CharsAndPositions.ContainsKey(required))
                    {
                        //We have a position
                        RectangleF Position = CharsAndPositions[required];
                        //Write margin
                        writeFile.Write((sbyte)0);
                        //Calculate width and height
                        float Width = (19f * Position.Width) / 91.9552f;
                        float Height = (16f * Position.Height) / 83.0464f;
                        //Calculate the top and right margins
                        float TopSpacing = (-17f * Height) / 16f;
                        float RightSpacing = (17f * Width) / 19f;
                        writeFile.Write(Convert.ToSByte(TopSpacing + TopOffset));
                        //Check
                        if (required == 32)
                        {
                            //Override space
                            writeFile.Write(Convert.ToSByte(RightSpacing));
                        }
                        else
                        {
                            writeFile.Write(Convert.ToSByte(RightSpacing + RightOffset));
                        }
                        //Write width and height
                        writeFile.Write(Convert.ToByte(Width));
                        writeFile.Write(Convert.ToByte(Height));
                        writeFile.Write((byte)0);
                        //Write UVLeft
                        float UVLeft = Convert.ToSingle(Position.X / BitmapWidth);
                        //Threshold
                        UVLeft += 0.01f;
                        //Write
                        writeFile.Write(UVLeft);
                        //Write UVTop
                        float UVTop = Convert.ToSingle(Position.Y / BitmapHeight);
                        //Threshhold
                        UVTop += 0.01f;
                        //Write
                        writeFile.Write(UVTop);
                        //Write UVRight
                        float UVRight = (Position.Width / BitmapWidth) + UVLeft;
                        //Threshhold
                        UVRight -= 0.01f;
                        //Write
                        writeFile.Write(UVRight);
                        //Write UVBottom
                        float UVBottom = (Position.Height / BitmapHeight) + UVTop;
                        //Threshhold
                        UVBottom -= 0.01f;
                        //Write
                        writeFile.Write(UVBottom);
                    }
                }
                writeFile.Write(StringToBytes(FontName));
                writeFile.Write(StringToBytes(FontMaterial));
            }
        }

        private const string normalFont = "normalFont";
        private const string boldFont = "boldFont";
        private const string bigFont = "bigFont";
        private const string extraBigFont = "extraBigFont";
        private const string objectiveFont = "objectiveFont";
        private const string smallFont = "smallFont";
        private const string consoleFont = "consoleFont";
        private const string fontMaterial = "gamefonts2pc";

        private static string GetFontFilesDirectory(string DirectoryToDump, string FontFamily)
        {
            string DirectoryName = FontFamily.Replace(' ', '_');
            return Path.Combine(DirectoryToDump, DirectoryName);
        }
        public static void GenerateSpriteSheetFontThing(
            string FontFamily,
            string FileName,
            int FontSize = 72,
            int TopOffset = 4,
            int RightOffset = -4,
            int BitmapWidth = 1024,
            int BitmapHeight = 2048,
            string FontMaterial = fontMaterial,
            Charset charset = Charset.DefaultCharset)
        {
            string characters = charset == Charset.CyrillicCharset ? CyrillicChar : GenericChar;

            //Setup a bitmap image
            using (Bitmap sheet = new Bitmap(BitmapWidth, BitmapHeight))
            {
                using (Graphics graphics = Graphics.FromImage(sheet))
                {
                    //Set transparent BG
                    graphics.Clear(Color.FromArgb(0, 0, 0, 0));
                    //A list of positions
                    Dictionary<short, RectangleF> CharsAndPositions = new Dictionary<short, RectangleF>();
                    //Setup font
                    using (Font font = new Font(FontFamily, FontSize, System.Drawing.FontStyle.Regular, GraphicsUnit.Point))
                    {
                        //X and Y
                        float X = 0;
                        float Y = 0;

                        //Loop
                        foreach (char c in characters.ToCharArray())
                        {
                            //95x125
                            SizeF SizeOfChar = graphics.MeasureString(c.ToString(), font);
                            char[] cc = { c };
                            short CharASCII = Encoding.GetEncoding(1251).GetBytes(cc)[0];
                            if ((X + SizeOfChar.Width + 4) > BitmapWidth)
                            {
                                Y += 126;
                                X = 0;
                            }
                            if (!CharsAndPositions.ContainsKey(CharASCII))
                            {
                                CharsAndPositions.Add(CharASCII, new RectangleF(X, Y, SizeOfChar.Width, SizeOfChar.Height));
                            }
                            //Once we get the size, we must calculate the thingy
                            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                            graphics.DrawString(c.ToString(), font, new SolidBrush(Color.White), new PointF(X, Y));
                            X += SizeOfChar.Width;
                        }
                        //Loop through required
                        string DirectoryToDump = Path.GetDirectoryName(FileName);
                        string FontFilesDirectory = GetFontFilesDirectory(DirectoryToDump, FontFamily);
                        if (!Directory.Exists(FontFilesDirectory))
                        {
                            Directory.CreateDirectory(FontFilesDirectory);
                        }
                        WriteToFile(DirectoryToDump, Path.Combine(FontFilesDirectory, normalFont), CharsAndPositions, TopOffset, RightOffset, BitmapWidth, BitmapHeight, fontStringPrefix + normalFont, fontStringPrefix + FontMaterial);
                        WriteToFile(DirectoryToDump, Path.Combine(FontFilesDirectory, boldFont), CharsAndPositions, TopOffset, RightOffset, BitmapWidth, BitmapHeight, fontStringPrefix + boldFont, fontStringPrefix + FontMaterial);
                        WriteToFile(DirectoryToDump, Path.Combine(FontFilesDirectory, bigFont), CharsAndPositions, TopOffset, RightOffset, BitmapWidth, BitmapHeight, fontStringPrefix + bigFont, fontStringPrefix + FontMaterial);
                        WriteToFile(DirectoryToDump, Path.Combine(FontFilesDirectory, extraBigFont), CharsAndPositions, TopOffset, RightOffset, BitmapWidth, BitmapHeight, fontStringPrefix + extraBigFont, fontStringPrefix + FontMaterial);
                        WriteToFile(DirectoryToDump, Path.Combine(FontFilesDirectory, objectiveFont), CharsAndPositions, TopOffset, RightOffset, BitmapWidth, BitmapHeight, fontStringPrefix + objectiveFont, fontStringPrefix + FontMaterial);
                        WriteToFile(DirectoryToDump, Path.Combine(FontFilesDirectory, smallFont), CharsAndPositions, TopOffset, RightOffset, BitmapWidth, BitmapHeight, fontStringPrefix + smallFont, fontStringPrefix + FontMaterial);
                        WriteToFile(DirectoryToDump, Path.Combine(FontFilesDirectory, consoleFont), CharsAndPositions, TopOffset, RightOffset, BitmapWidth, BitmapHeight, fontStringPrefix + consoleFont, fontStringPrefix + FontMaterial);
                    }
                }
                //Save the bitmap
                System.Windows.Media.Imaging.WriteableBitmap writeableBitmap = new System.Windows.Media.Imaging.WriteableBitmap(System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(sheet.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions()));
                //Write it
                TgaWriter.Write(writeableBitmap, File.Create(FileName));
            }
        }

        private static System.Windows.Media.FontFamily TransformFontFamily(System.Drawing.FontFamily family)
        {
            return new System.Windows.Media.FontFamily(family.Name);
        }

        public static bool SupportsCyrillic(FontFamily family)
        {
            var mediaFamily = TransformFontFamily(family);
            var typefaces = mediaFamily.GetTypefaces();
            bool supportsCyrillic = false;
            foreach (Typeface typeface in typefaces)
            {
                GlyphTypeface glyph;
                typeface.TryGetGlyphTypeface(out glyph);
                if (glyph is null)
                {
                    continue;
                }

                IDictionary<int, ushort> characterMap = glyph.CharacterToGlyphMap;

                //TODO: research why doesn't work with GenericChar
                const string abc = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
                supportsCyrillic = supportsCyrillic || abc.All(ch => characterMap.ContainsKey(ch));
            }
            return supportsCyrillic;
        }
    }
}
