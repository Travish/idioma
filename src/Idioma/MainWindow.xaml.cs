﻿using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Idioma
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void DoThing_Click(object sender, RoutedEventArgs e)
        {
        }

        private static FontGenerator.Charset StrToCharset(string str)
        {
            if (str == "cyrillic")
            {
                return FontGenerator.Charset.CyrillicCharset;
            }
            return FontGenerator.Charset.DefaultCharset;
        }

        private void GenerateDefault_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //Generate
            using (System.Windows.Forms.SaveFileDialog saveFile = new System.Windows.Forms.SaveFileDialog() { Filter = "TGA (*.tga)|*.tga", FileName = this.MaterialNameTextBox.Text })
            {
                saveFile.FileName = this.MaterialNameTextBox.Text;
                if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    FontGenerator.GenerateSpriteSheetFontThing(((CheckBox)this.FontSelection.SelectedItem).Content.ToString(), saveFile.FileName,
                        Convert.ToInt32(this.ScaleFontSize.Text), Convert.ToInt32(this.TopOffsetSize.Text),
                        this.RightOffsetSize.Value.Value, Convert.ToInt32(((System.Windows.Controls.ComboBoxItem)BitmapWidthComboBox.SelectedValue).Content),
                        Convert.ToInt32(((System.Windows.Controls.ComboBoxItem)BitmapHeightComboBox.SelectedValue).Content), MaterialNameTextBox.Text,
                        StrToCharset(((System.Windows.Controls.ComboBoxItem)OutputCharsetComboBox.SelectedValue).Content.ToString()));
                    MessageBox.Show("The font has been saved!", "Idioma", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        private void ScaleFontSize_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Loop through fonts
            InstalledFontCollection fonts = new InstalledFontCollection();
            foreach (System.Drawing.FontFamily family in fonts.Families)
            {
                var checkBox = new CheckBox();
                checkBox.Content = family.Name;
                checkBox.IsChecked = FontGenerator.SupportsCyrillic(family);
                checkBox.Focusable = false; // Make it read only
                checkBox.IsHitTestVisible = false; // Make it read only
                this.FontSelection.Items.Add(checkBox);
            }
            this.FontSelection.SelectedIndex = 0;
        }
    }
}
